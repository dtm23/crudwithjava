package dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//Database connection
public class DConnection {
	protected String dbName = "crud";
	protected String userName = "root";
	protected String password  ="";
	Connection conn = null;
	String driverName = "com.mysql.jdbc.Driver";
	
	public void dbConnection() throws Exception{
		try{
			Class.forName(driverName).newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://localhost/"+ dbName ,userName, password);
			System.out.println("Database is connected !");
			
		}catch(Exception e){
			System.out.println("DB Not Connected - Error:"+e);
		}
	}
//	public void closeDbConnection(){
//		try {
//			conn.close();
//			System.out.println("DB closed");
//		} catch (SQLException e) {
//			System.out.println("Error closing DB connection -" + e.getMessage());
//		}
//	}
	
}
