
import dbConnection.DConnection;
import UI.LoginUI;
public class Main{
	static DConnection con = new DConnection();
	static LoginUI login ;

	public static void main(String[] args) throws InterruptedException {
		
		try {
			con.dbConnection();
			Thread.sleep(1000);

			login = new LoginUI();
			login.showLoginFrame();
			
		} catch (Exception e) {
			System.out.println("Error = " +e.getMessage());
		}
		
		
	}

}
