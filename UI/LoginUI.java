package UI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class LoginUI extends JFrame {
	private Statement st;
	private ResultSet rs;
	private JLabel labelID,labelPassword;
	private ImageIcon userIdIcon,userPasswordIcon,submitIcon;
    JPanel panel = new JPanel(new BorderLayout());
	public void showLoginFrame(){
		JFrame frame = new JFrame("CRUD - Dvndra");
		frame.setSize(300, 320); //width , height
		frame.setLocationRelativeTo(null); // locate frame to the middle
		frame.getContentPane().setBackground(Color.WHITE); // change the frame color
		frame.setResizable(false); // frame cannot be resize - disable max button
		frame.setLayout(null);
		
		userIdIcon = new ImageIcon("user.png");
		userPasswordIcon = new ImageIcon("pass.png");
		submitIcon = new ImageIcon("submit.png");

		labelID = new JLabel("User ID",userIdIcon,SwingConstants.CENTER);
		//frame x, frame y, object x, object y
		labelID.setBounds(300/2-60, 60, 100, 25); 
		
		JTextField tfuser = new JTextField();
		tfuser.setBounds(300/2-90, 90, 180, 25);
		tfuser.setHorizontalAlignment(JTextField.CENTER); // 

		
		labelPassword = new JLabel("Password",userPasswordIcon,SwingConstants.CENTER);
		labelPassword.setBounds(300/2-60, 135, 100, 25);


		JPasswordField  tfpassword = new JPasswordField();		
		tfpassword.setBounds(300/2-90, 165, 180, 25);
		tfpassword.setHorizontalAlignment(JTextField.CENTER); // center a text 
		
		JButton submit = new JButton(submitIcon);
		submit.setBounds(300/2-90,200, 70, 30);
		submit.getColorModel().getColorSpace();
		submit.setFocusPainted(false);
		submit.setBorderPainted(true);
		submit.setContentAreaFilled(false);
		
		submit.addActionListener(new ActionListener(){

			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent ae) {
				try{
					String uid = tfuser.getText().trim();
					String uspass = tfpassword.getText().trim();
	
					String sql = "select * from users where userid='"+uid+" 'and password='"+uspass +"'";
					rs = st.executeQuery(sql);
					System.out.println("HERE");
					int count = 0;
					while(rs.next()){
						count++;
					}
					if(count == 1){
						JOptionPane.showMessageDialog( null, "User Found and Access Granted");
					}else if(count > 1){
						JOptionPane.showMessageDialog( null, "Duplicate User Found! User Has been Blocked");
					}else{
						JOptionPane.showMessageDialog( null, "User Not Found");
					}
					
				}catch(Exception e){
					System.out.println("Error while retriving from fields " + e.getMessage());
				}
			}
			
		});
				
		JLabel txtforgot = new JLabel("forgot password!");
		txtforgot.setBounds(300/2-18, 208, 150, 25);

		frame.add(labelID);
		frame.add(tfuser);
		frame.add(labelPassword);
		frame.add(tfpassword);
		frame.add(submit);
		frame.add(txtforgot);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		frame.setVisible(true);
	}
	
}	
